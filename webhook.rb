#!/usr/bin/ruby

# Copyright (C) 2018, Joerg Jaspert <joerg@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Load required libraries
# Eventmachine for deferred execution
require 'eventmachine'
# We use sinatra here
require 'sinatra/base'
require "sinatra"
# Allow to read app settings from yaml file
require "sinatra/config_file"
# And the webserver framework for eventmachine
require 'thin'
# We define our logging stuff
require 'logger'

# Beware, a global variable.
# Store our main directory so we can use it later to easily find things.
$maindir=File.expand_path(File.dirname(__FILE__))

# Load our config file
config_file File.join($maindir, 'config.yml')

# Setup our application environment
def run()
  # Start he reactor
  EM.run do
    dispatch = Rack::Builder.app do
      map '/' do
        run SalsaWebHook.new
      end
    end

    # Start the web server.
    Rack::Server.start({
                         app:    dispatch,
                         server: settings.servertype,
                         Host:   settings.bind,
                         Port:   settings.port,
                         signals: false,
                       })
  end # EM.run
end # def run

# And here goes our actual webapp (hook) logic
class SalsaWebHook < Sinatra::Application

  # Set some defaults, run once at beginning
  configure do
    # queue request for background thread
    set :threaded, true

    # Do not show exceptions
    set :show_exceptions, false
  end

  # Some defaults.
  before do
    # Return plain text. Gitlab talks with us, gitlab doesn't care
    # what we spit out. Make it plaintext...
    content_type :txt
  end

  # This function checks should we ignore the call of web-hook or not.
  def skip_push?(data)
    # Ignore non push updates.
    return true if data["object_kind"] != "push"
    # Skip pushes into ignored branches.
    ignored_namespaces = params.fetch("ignored-namespaces", "wip,pu,people").downcase.split(",")
    prefixes = ignored_namespaces.product("/_-".chars).map { |a, b| a + b }
    branch_name = data["ref"].downcase.sub(/^refs\/heads\//, "")
    ignored_namespaces.include?(branch_name) or branch_name.start_with?(*prefixes)
  end

  def scan_bugs(text)
    text.scan(/Closes:\s+(?:Bug)?#(?:(\d{4,8})\b)(?:,?\s*(?:Bug)?#(?:(\d{4,8})\b))*/i)
  end

  # The / endpoint doesn't do anything special, just tells users they
  # are wrong here. Everything that actually does something useful is in
  # a plugin in the plugins dir.
  post '/' do
    # A human trying to talk to us - or someone misconfiguring their hook
    <<-EOERROR
You reached a webhook. That means either you try this as a user in a browser,
which does not do any good, or you misconfigured your webhook settings.
EOERROR
  end
  # And for users with a webbrowser, we support get.
  get '/' do
    # A human trying to talk to us - or someone misconfiguring their hook
    <<-EOERROR
You reached a webhook. That means either you try this as a user in a browser,
which does not do any good, or you misconfigured your webhook settings.
EOERROR
  end

  # Now load each plugin, which should declare their route and code
  settings.plugins.each do |plugin|
    require_relative "plugins/#{plugin}"
  end
end # class SinatraWebHook

# And last line - run this whole thingie
run
